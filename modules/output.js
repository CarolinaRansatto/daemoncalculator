const GifHelper = require("./gifHelper");
const Utils = require("./utils");

module.exports = {
    sendResult(message, value, critical=0) {
        value = Math.ceil(value);
        const rolled = Utils.roll(100, 1);

        let criticalValue = 0;
        if (rolled < critical) {
            criticalValue = 1;
            GifHelper.getSuccess(message);
        }
        else if (rolled >= 95) {
            criticalValue = 2;
            GifHelper.getFail(message);
        }

        message.reply(`${value}%, rolou ${rolled} = ${rolled <= value && rolled < 95 ? 'sucesso' : 'falha'}${criticalValue === 1 ? ' crítico' : criticalValue === 2 ? ' crítica' : ''}`);
    },

    sendUserError(message) {
        const errorMessages = require('fs').readFileSync('modules/error_messages.txt', 'utf-8').split('\n');
        message.reply(errorMessages[Utils.roll(errorMessages.length)]);
    },

    sendRequestError(message, error) {
        message.channel.send('Opa, deu ruim: ' + error)
    }
}