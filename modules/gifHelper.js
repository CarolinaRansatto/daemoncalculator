const Utils = require('./utils')
const Output = require('./output')
const GphApiClient = require('giphy-js-sdk-core');
const giphyClient = GphApiClient(process.env.GIPHY_TOKEN);

const start = ['its time'];
const end = ['the end', 'bye'];
const success = ['sucess', 'not bad', 'victory dance', 'nice'];
const fail = ['ambulance', 'failure', 'died', 'funeral'];

module.exports = {
    getGif(message, searchValue) {
        giphyClient.search('gifs', { "q": searchValue }).then((response) => {
            message.channel.send(response.data[Utils.roll(25, 0)].embed_url);
        }).catch((error) => {
            Output.sendRequestError(message, error);
        })
    },

    getStart(message) {
        this.getGif(message, start[Utils.roll(start.length)])
    },

    getEnd(message) {
        this.getGif(message, end[Utils.roll(end.length)])
    },
    
    getSuccess(message) {
        this.getGif(message, success[Utils.roll(success.length)])
    },

    getFail(message) {
        this.getGif(message, fail[Utils.roll(fail.length)])
    }
}