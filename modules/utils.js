module.exports = {
    roll(max, add=0) {
        return Math.floor(Math.random() * max) + add;
    }
}