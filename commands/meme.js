const request = require('request');
const Output = require('../modules/output');

const memeList = {
    envy: '112126428',
    batman: '438680',
    buttons: '87743020',
    aliens: '101470',
    change: '129242436',
    wait: '4087833',
    exit: '124822590',
    surprised: '155067746',
    drake: '181913649',
    sponge: '102156234',
    button: '119139145',
    genio: '89370399',
    escobar: '80707627',
    gravando: '148909805',
    nascimento: '231134849',
    math: '80996545',
    birl: '66267507'
};

module.exports = {
    name: 'm',
    description: `Gera um meme. OBS: Note que nem todos os memes usam 2 textos. Alguns ids de memes mais populares: https://api.imgflip.com/popular_meme_ids. Também pode usar essas palavras-chaves no lugar do id: ${Object.keys(memeList).join(', ')}.`,
    args: true,
    usage: '[id do meme] | [texto 1] | [texto 2]',
    execute(message, args) {
        args = message.content.substring(3).split(' | ');
        const id = Object.keys(memeList).includes(args[0]) ? memeList[args[0]] : args[0];
        request.post({
            url: 'https://api.imgflip.com/caption_image', form: {
                template_id: id,
                username: process.env.IMGFLIP_LOGIN,
                password: process.env.IMGFLIP_PASSWORD,
                text0: args[1],
                text1: args[2]
            }
        }, function (error, response, body) {
            body = JSON.parse(body);
                console.log(`-${args[0]}-`);
            if (body.success) {
                message.channel.send(body.data.url);
            }
            else {
                Output.sendRequestError(message, error);
            }
        });
    }
}