const Output = require("../modules/output")

module.exports = {
    name: 'a',
    description: 'Rola teste de atributo',
    args: true,
    usage: '[ativo] [passivo]',
    execute(message, args) {
        if (args.length !== 2) {
            Output.sendUserError(message);
            return;
        }
        args = args.map(a => parseInt(a));
        Output.sendResult(message, (args[0] - args[1]) * 5 + 50);
    }
}