const Utils = require("../modules/utils");

module.exports = {
    name: 'r',
    description: 'Rola um d100',
    execute(message, args) {
        message.reply(`rolou ${Utils.roll(100, 1)}`);
    }
}