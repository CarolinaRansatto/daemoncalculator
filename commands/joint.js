const Output = require("../modules/output")

module.exports = {
    name: 'c',
    description: 'Rola teste conjunto',
    args: true,
    usage: '[atributo 1] [atributo 2] ... [atributo n]',
    execute(message, args) {
        if (args.length < 2) {
            Output.sendUserError(message);
            return;
        }
        args = args.map(a => parseInt(a));
        const max = Math.max(...args);
        const rest = args.reduce((a, b) => a + b, 0) - max;
        Output.sendResult(message, max + (rest / 2));
    }
}