module.exports = {
    name: 'help',
    description: 'Lista todos os comandos',
    execute(message, args) {
        const commands = message.client.commands.map(command => `$${command.name} ${command.usage || ''} - ${command.description}`);
        message.channel.send(commands.join('\n'));
    }
}