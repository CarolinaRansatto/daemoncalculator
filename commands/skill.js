const Output = require("../modules/output")

module.exports = {
    name: 'p',
    description: 'Rola teste de perícia',
    args: true,
    usage: '[ataque] [defesa]',
    execute(message, args) {
        if (args.length !== 2) {
            Output.sendUserError(message)
            return
        }
        args = args.map(a => parseInt(a));
        Output.sendResult(message, args[0] + 50 - args[1], Math.ceil(args[0] / 4));
    }
}