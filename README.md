# Sobre

Bot do Discord feito para rolar testes de perícia, atributo e testes conjuntos de Daemon. Responde a erros com mensagens engraçadas (e não muito amigáveis).

Essa página é para o uso de desenvolvedores. Para usar o bot no Discord, [use esse link](https://discordapp.com/api/oauth2/authorize?client_id=697522935262478460&permissions=67584&scope=bot).

Esse repositório é de código aberto e o código contido aqui pode ser usado, clonado, modificado, etc, para fins não lucrativos.

# Comandos

- $help - mostra os outros comandos disponíveis
- $p [perícia ativa] [perícia passiva] - rola teste de perícia
- $a [atributo ativo] [atributo passivo] - rola teste de atributo
- $c [atributo do jogador 1] [atributo do jogador 2] ... [atributo do jogador n] - rola teste conjunto

# Requerimentos

- node v12.6.2
- discordjs
- dotenv

# Informações técnicas
O bot responde a qualquer frase começando com '$'. Se for um comando mal formado, ele utiliza uma das mensagens de erro do arquivo error_messages.txt. O arquivo é lido toda vez que um erro vai ser mostrado, para que as frases possam ser modificadas sem ser necessário reiniciar o bot, mas isso pode causar lentidão.

Para rodar, o bot precisa de um token de app do discord salvo em um arquivo .env no seguinte formato:

```sh
DISCORD_TOKEN=seu_token_aqui
```