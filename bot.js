require('dotenv').config();
const fs = require('fs');
const Discord = require('discord.js');
const Output = require("./modules/output");
const GifHelper = require("./modules/gifHelper");

const client = new Discord.Client();
client.commands = new Discord.Collection();
const commandFiles = fs.readdirSync('./commands')

for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	client.commands.set(command.name, command);
}

client.on('ready', () => {
	console.log(`${client.user.tag} ready.`);
});

client.on('message', message => {
	if (message.content.toLowerCase().includes('ping'))
		message.channel.send('pong');

	if (message.content.toLowerCase().includes(':craig:, entrar'))
		GifHelper.getStart(message);

	if (message.content.toLowerCase().includes(':craig:, sair'))
		GifHelper.getEnd(message);

	if (!message.content.startsWith('$') || message.author.bot)
		return;

	const args = message.content.slice(1).split(/ +/);
	const command = args.shift().toLowerCase();

	if (!client.commands.has(command)) {
		Output.sendUserError(message);
		return;
	}

	try {
		client.commands.get(command).execute(message, args);
	} catch (e) {
		Output.sendUserError(message);
	}
});

client.login(process.env.token);